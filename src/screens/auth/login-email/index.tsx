import * as React from 'react';
import {StyleSheet, ActivityIndicator, ScrollView} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import Theme from '../../../assets/theme';
import UiButton from '../../../components/_partials/button';
import UiInput from '../../../components/_partials/input';

import http from '../../../helpers/http';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {login} from '../../../redux/actions';
import {User} from '../../../assets/models';

export interface AuthLoginEmailPageProps {
  navigation: any;
  _login: Function;
}

const AuthLoginEmailPage = ({navigation, _login}: AuthLoginEmailPageProps) => {
  const insets = useSafeAreaInsets();

  const [loading, setLoading] = React.useState<boolean>(false);

  const [email, setEmail] = React.useState<string>();

  const _handleNext = async () => {
    try {
      if (loading) {
        return;
      }
      setLoading(true);
      await AsyncStorage.removeItem('accessToken');
      const response = await http.post('/login/email', {
        email: email?.trim()?.toLowerCase(),
      });
      navigation?.navigate('LoginPassword', {
        token: response?.data?.result,
        email: email?.trim()?.toLowerCase(),
      });
      setLoading(false);
    } catch (_error: any) {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    const focus = navigation?.addListener('focus', async () => {
      try {
        const accessToken = await AsyncStorage.getItem('accessToken');
        if (accessToken?.trim()) {
          _login({
            name: 'User',
            token: accessToken,
          });
        }
      } catch (_error) {}
    });
    return focus;
  }, [_login, navigation]);

  return (
    <>
      <ScrollView
        keyboardDismissMode="interactive"
        keyboardShouldPersistTaps="handled"
        bounces={false}
        showsVerticalScrollIndicator={false}
        style={[styles.container]}
        contentContainerStyle={[
          styles.containerContent,
          {paddingTop: insets?.top + 10, paddingBottom: insets?.bottom + 10},
        ]}>
        <UiInput
          style={[styles.input]}
          value={email}
          disabled={!!loading}
          label="EMAIL"
          keyboardType="email-address"
          onSubmitEditing={_handleNext}
          onChangeText={setEmail}
        />
        <UiButton disabled={!!loading} onPress={_handleNext}>
          {loading ? (
            <ActivityIndicator size="small" color={Theme.colors.white} />
          ) : (
            'NEXT'
          )}
        </UiButton>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerContent: {
    width: '100%',
    height: '100%',
    backgroundColor: Theme?.colors?.white,
    paddingHorizontal: 31,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    marginBottom: 65,
  },
});

const mapDispatchToProps = (dispatch: any) => ({
  _login: (payload: User | null | undefined = null) => dispatch(login(payload)),
});

export default connect(() => ({}), mapDispatchToProps)(AuthLoginEmailPage);
