import {User} from '../../assets/models';
import {SET_CURRENT_USER} from '../types';
import AsyncStorage from '@react-native-async-storage/async-storage';

import * as navigation from '../../helpers/navigation';

const setCurrentUser = (payload: User | null | undefined = null) => {
  return {type: SET_CURRENT_USER, payload};
};

export const setUser =
  (payload: User | null | undefined = null) =>
  (dispatch: any) => {
    dispatch(setCurrentUser(payload));
  };

export const login =
  (payload: User | null | undefined = null) =>
  async (dispatch: any) => {
    await AsyncStorage.setItem('accessToken', `${payload?.token}`);
    dispatch(setUser(payload));
    navigation?.reset({
      index: 0,
      routes: [{name: 'HomePage'}],
    });
  };

export const logout = () => async (dispatch: any) => {
  await AsyncStorage.removeItem('accessToken');
  dispatch(setUser(null));
  navigation?.reset({
    index: 0,
    routes: [{name: 'LoginEmail'}],
  });
};
