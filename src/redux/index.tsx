import {configureStore} from '@reduxjs/toolkit';
import {mainReducer} from './reducers';
import thunk from 'redux-thunk';

const store = configureStore({
  reducer: mainReducer,
  middleware: [thunk] as const,
});

export type AppDispatch = typeof store.dispatch;

export default store;
