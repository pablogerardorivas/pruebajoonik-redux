import {User} from '../../assets/models';

export const SET_CURRENT_USER = 'SET_CURRENT_USER';

interface SetCurrentUser {
  type: typeof SET_CURRENT_USER;
  payload?: User | null;
}

export type AuthActionTypes = SetCurrentUser;
