import {User} from '../../assets/models';
import {AuthActionTypes, SET_CURRENT_USER} from '../types';

interface AuthState {
  currentUser?: User | null;
}

const initialState: AuthState = {
  currentUser: null,
};

export const authReducer = (
  state: AuthState = initialState,
  action: AuthActionTypes,
): AuthState => {
  switch (action.type) {
    case SET_CURRENT_USER: {
      return {
        ...state,
        currentUser: action?.payload,
      };
    }
    default:
      return state;
  }
};
