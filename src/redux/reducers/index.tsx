import {combineReducers} from '@reduxjs/toolkit';
import {authReducer} from './auth.reducer';

export const mainReducer = combineReducers({
  auth: authReducer,
});

export type MainState = ReturnType<typeof mainReducer>;
