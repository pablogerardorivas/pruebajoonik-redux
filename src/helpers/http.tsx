import axios from 'axios';
import {showToast} from './toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import store from '../redux/';

import * as navigation from './navigation';
import {SET_CURRENT_USER} from '../redux/types';

export const globalHeaders = {
  Accept: 'application/json',
};
export const instance = axios.create({
  baseURL: 'https://api.joonik.com',
  headers: {
    Accept: 'application/json',
  },
});

instance.interceptors.request.use(
  async config => {
    const _config = {
      ...config,
    };

    const accessToken = await AsyncStorage.getItem('accessToken');
    if (accessToken) {
      _config.headers.Authorization = `Bearer ${accessToken}`;
    }

    return _config;
  },
  error => {
    return Promise.reject(error);
  },
);

const {dispatch} = store;
instance.interceptors.response.use(
  response => {
    return response;
  },
  async error => {
    if (error?.response?.status === 401) {
      await AsyncStorage.setItem('userToken', '');
      dispatch({type: SET_CURRENT_USER, payload: null});
      navigation?.reset({
        index: 0,
        routes: [{name: 'LoginEmail'}],
      });
    } else {
      if (
        typeof error?.response?.data?.error === 'string' &&
        error?.response?.data?.error?.trim()
      ) {
        showToast(error?.response?.data?.error?.trim(), {
          status: 'error',
        });
      }
    }
    return Promise.reject(error);
  },
);

export default instance;
